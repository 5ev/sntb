# Social Networks Tools Bot

## What the bot does?

Gives telegram users some tools to work with social networks

### Search instagram username by its ID

Can be useful when the user you are looking for has changed his username,
but you have his/her ID. The bot helps you find the new username by its ID.

### Get instagram user ID by its username

If you don't want to lose a user you can store his/her ID given by the bot.
If the user change his/her username the ID remains and the bot will
give you new username by the ID.

### New features

If you need a new feature you can create a new issue [here](https://gitlab.com/5ev/sntb/-/issues),
describe the feature, and we will consider to implement it in future releases.

Or you can describe the feature in email and send it to this address:
`incoming+5ev-sntb-26281077-bpqkdmgwunfbt39tl0lg757pa-issue@incoming.gitlab.com`
(GitLab will create issue based on your email automatically).

## Requirements

* JDK11 - OpenJDK HotSpot, for example.
* Token taken from [BotFather](https://t.me/BotFather) bot.
  Put the token into `token` system property.

## Where the bot lives?

It hosts on https://heroku.com
and available here https://t.me/socialnetworkstoolsbot
