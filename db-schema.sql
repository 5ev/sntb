create table log
(
    timestamp  varchar,
    level      varchar,
    logger     varchar,
    msg        varchar,
    stacktrace varchar
);

alter table log
    owner to etssexlylbernz;

---------------------------

create table insta_accounts
(
    user_name varchar not null
        constraint insta_accounts_pk
            primary key
);

alter table insta_accounts
    owner to etssexlylbernz;

------------------------------

create table headers
(
    insta_account_name varchar not null
        constraint headers_insta_accounts_user_name_fk
            references insta_accounts
            on update cascade on delete cascade,
    name               varchar not null,
    value              varchar not null
);

alter table headers
    owner to etssexlylbernz;

