package net.socialnetworkstools.bot.service;

public final class Text {

    private Text() {
    }

    public static String escape(String text) {
        return text
//            .replace("_", "\\_")
//            .replace("*", "\\*")
//            .replace("[", "\\[")
            .replace("`", "\\`");
    }

}
