package net.socialnetworkstools.bot.service;

import net.socialnetworkstools.bot.dao.LanguageDao;
import net.socialnetworkstools.bot.model.Language;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.inject.Inject;

/**
 * Internationalization
 *
 * https://www.baeldung.com/java-resourcebundle
 */
public class I18n {

    private final LanguageDao languageDao;

    @Inject
    I18n(LanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    public String msg(long chatId, String key, Object ... arguments) {

        String lang = languageDao.getLang(chatId);

        Locale locale = new Locale(lang);
        ResourceBundle bundle = ResourceBundle.getBundle("i18n.resources", locale);

        String pattern = bundle.getString(key);

        return MessageFormat.format(pattern, arguments);
    }

    public void setLang(long chatId, Language language) {
        languageDao.setLang(chatId, language.name().toLowerCase(Locale.ROOT));
    }
}
