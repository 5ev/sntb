package net.socialnetworkstools.bot.service;

import net.socialnetworkstools.bot.dao.InstaAccountsDao;
import net.socialnetworkstools.bot.exception.OutOfServiceException;
import net.socialnetworkstools.bot.model.InstaAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class InstaAccountService {

    private static final Logger LOGGER = LogManager.getLogger(InstaAccountService.class);

    private final List<InstaAccount> instaAccounts;
    private int currentIndex = 0;

    @Inject
    public InstaAccountService(InstaAccountsDao dao) {
        instaAccounts = dao.getInstaAccounts();
    }

    public InstaAccount get() {
        synchronized (instaAccounts) {
            int size = instaAccounts.size();
            if (size == 0) {
                throw new OutOfServiceException();
            }
            currentIndex = ++currentIndex % size;
            return instaAccounts.get(currentIndex);
        }
    }

    public void deactivate(String userName) {
        boolean deactivated;
        synchronized (instaAccounts) {
            deactivated = instaAccounts.removeIf(next -> next.getUserName().equals(userName));
        }
        if (deactivated) {
            LOGGER.warn("Instagram user {} is logged out and deactivated", userName);
        }
    }
}
