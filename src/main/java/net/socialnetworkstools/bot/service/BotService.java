package net.socialnetworkstools.bot.service;

import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.commandhandlers.CommandHandlerResolver;
import net.socialnetworkstools.bot.dao.ChatStateDao;
import net.socialnetworkstools.bot.exception.InvalidInputException;
import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InteractionMessage;
import net.socialnetworkstools.bot.model.InteractionResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;

public class BotService {

    private static final Logger LOGGER = LogManager.getLogger(BotService.class);

    private final CommandHandlerResolver commandHandlerResolver;
    private final ChatStateDao chatStateDao;
    private final I18n i18n;

    @Inject
    BotService(CommandHandlerResolver commandHandlerResolver, ChatStateDao chatStateDao, I18n i18n) {
        this.commandHandlerResolver = commandHandlerResolver;
        this.chatStateDao = chatStateDao;
        this.i18n = i18n;
    }

    public InteractionMessage process(long chatId, String input) {
        try {
            return Command.find(input)
                .map(commandHandlerResolver::resolve)
                .map(commandHandler -> {
                    InteractionResult result = commandHandler.next(chatId, null, null);
                    chatStateDao.setChatState(chatId, commandHandler.responsibleFor(), result.getStep());
                    return result.getMessage();
                })
                .or(() -> chatStateDao.getChatState(chatId)
                    .map(chatState -> {
                        Command command = chatState.getCommand();
                        CommandHandler commandHandler = commandHandlerResolver.resolve(command);
                        InteractionResult result = commandHandler.next(chatId, chatState.getStep(), input);
                        chatStateDao.setChatState(chatId, commandHandler.responsibleFor(), result.getStep());
                        return result.getMessage();
                    })
                )
                .orElseGet(() -> {
                    String msg = i18n.msg(chatId, "bot-service.unknown-cmd");
                    return InteractionMessage.of(msg, Command.asButtons());
                });

        } catch (InvalidInputException iie) {
            return InteractionMessage.of(iie.getMessage());
        } catch (Exception e) {
            LOGGER.error("", e);
            return InteractionMessage.of("Ups...\n"
                + "Something went wrong.\n"
                + "See log.");
        }
    }

}
