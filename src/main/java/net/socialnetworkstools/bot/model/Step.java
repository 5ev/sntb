package net.socialnetworkstools.bot.model;

public interface Step {

    default boolean isAsync() {
        return false;
    }

    String name();

    /**
     * Return next constant except very first in the array.
     * For example, if enum has 3 constants (C1, C2, C3) then the method will return
     * assert C1.next() == C2
     * assert C2.next() == C3
     * assert C3.next() == C2
     */
    default Enum<?> next() {
        Enum<?> current = (Enum<?>) this;
        Enum<?>[] enumConstants = current.getClass().getEnumConstants();

        int i = current.ordinal();
        if (i == enumConstants.length - 1 && enumConstants.length > 1) {
            i++;
        }
        int n = (i + 1) % enumConstants.length;
        return enumConstants[n];
    }
}
