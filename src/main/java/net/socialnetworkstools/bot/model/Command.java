package net.socialnetworkstools.bot.model;

import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public enum Command {

    START("/start", false),
    HELP("/help", true),
    INSTA_GET_ID("/instagetid", true),
    INSTA_GET_USERNAME("/instagetusername", true),
    LANGUAGE("/language", true);

    private final String id;
    private final boolean isButton;

    public static Optional<Command> find(String text) {
        return Stream.of(values())
            .filter(item -> item.id.equalsIgnoreCase(text))
            .findAny();
    }

    public static String[] asButtons() {
        return Stream.of(values())
            .filter(cmd -> cmd.isButton)
            .map(cmd -> cmd.id)
            .toArray(String[]::new);
    }
}
