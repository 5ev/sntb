package net.socialnetworkstools.bot.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class ChatState {
    private final Command command;
    private final String step;
}
