package net.socialnetworkstools.bot.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class InteractionResult {
    private final String step;
    private final InteractionMessage message;

    public static InteractionResult of(String step, String text, String[] buttonTexts) {
        return new InteractionResult(step, InteractionMessage.of(text, buttonTexts));
    }

    public static InteractionResult of(String step, String text) {
        return new InteractionResult(step, InteractionMessage.of(text));
    }
}

