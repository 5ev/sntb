package net.socialnetworkstools.bot.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/** My accounts to view others from */
@Getter
@AllArgsConstructor(staticName = "of")
public class InstaAccount {

    private String userName;
    private List<Header> headers;

    /** Headers for HTTP requests to insta host */
    @Getter
    @AllArgsConstructor(staticName = "of")
    public static class Header {
        private String name;
        private String value;
    }
}
