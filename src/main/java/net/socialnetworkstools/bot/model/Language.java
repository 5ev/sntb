package net.socialnetworkstools.bot.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public enum Language {

    RU("Русский"),
    EN("English");

    final String name;

    public static String[] asButtons() {
        return Stream.of(values())
            .map(lang -> lang.name)
            .toArray(String[]::new);
    }

    public static Optional<Language> find(String name) {
        return Stream.of(values())
            .filter(lang -> lang.name.equalsIgnoreCase(name))
            .findAny();
    }
}
