package net.socialnetworkstools.bot.exception;

/** If the bot is not able to work. For example, wrong settings or necessary accounts are logged out. */
public class OutOfServiceException extends RuntimeException {
}
