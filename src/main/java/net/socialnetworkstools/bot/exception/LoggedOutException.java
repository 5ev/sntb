package net.socialnetworkstools.bot.exception;

import lombok.Getter;

/** Exception throwing when a user sending requests is logged out */
public class LoggedOutException extends RuntimeException {

    @Getter
    private final String userName;

    public LoggedOutException(String userName) {
        this.userName = userName;
    }
}
