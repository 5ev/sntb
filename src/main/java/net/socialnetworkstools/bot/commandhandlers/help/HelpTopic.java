package net.socialnetworkstools.bot.commandhandlers.help;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.socialnetworkstools.bot.model.Step;

import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

@RequiredArgsConstructor
public enum HelpTopic implements Step {

    WELCOME("help.welcome.label", false, "help.welcome.content"),
    ABOUT("help.about.label", true, "help.about.content"),
    TRUST("help.trust.label", true, "help.trust.content");

    private final String label;
    private final boolean isButton;
    @Getter
    private final String content;

    static String[] asButtons(UnaryOperator<String> formattingFunction) {
        return Stream.of(values())
            .filter(topic -> topic.isButton)
            .map(topic -> formattingFunction.apply(topic.label))
            .toArray(String[]::new);
    }

    static Optional<HelpTopic> find(String label, UnaryOperator<String> formattingFunction) {
        return Stream.of(values())
            .filter(topic -> formattingFunction.apply(topic.label).equalsIgnoreCase(label))
            .findAny();
    }
}
