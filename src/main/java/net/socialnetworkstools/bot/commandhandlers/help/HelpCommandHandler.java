package net.socialnetworkstools.bot.commandhandlers.help;

import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InteractionResult;

public class HelpCommandHandler extends CommandHandler {

    private final I18n i18n;

    public HelpCommandHandler(I18n i18n) {
        this.i18n = i18n;
    }

    @Override
    public Command responsibleFor() {
        return Command.HELP;
    }

    @Override
    public InteractionResult next(long chatId, String step, String input) {
        HelpTopic topic = HelpTopic.find(input, key -> i18n.msg(chatId, key))
            .orElse(HelpTopic.WELCOME);
        return InteractionResult.of(step, i18n.msg(chatId, topic.getContent()), HelpTopic.asButtons(key -> i18n.msg(chatId, key)));
    }
}
