package net.socialnetworkstools.bot.commandhandlers.start;

import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InteractionResult;

public class StartCommandHandler extends CommandHandler {

    private final I18n i18n;

    public StartCommandHandler(I18n i18n) {
        this.i18n = i18n;
    }

    @Override
    public Command responsibleFor() {
        return Command.START;
    }

    @Override
    public InteractionResult next(long chatId, String step, String input) {
        return InteractionResult.of("", i18n.msg(chatId, "start-command-handler.welcome"), Command.asButtons());
    }
}
