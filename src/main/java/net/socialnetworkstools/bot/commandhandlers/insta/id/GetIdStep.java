package net.socialnetworkstools.bot.commandhandlers.insta.id;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.socialnetworkstools.bot.model.Step;

@RequiredArgsConstructor
@Getter
public enum GetIdStep implements Step {
    START(false),
    RESOLVE(true);

    private final boolean async;
}
