package net.socialnetworkstools.bot.commandhandlers.insta.username;

import static java.lang.Character.isDigit;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.service.InstaAccountService;
import net.socialnetworkstools.bot.exception.LoggedOutException;
import net.socialnetworkstools.bot.service.Text;
import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InstaAccount;
import net.socialnetworkstools.bot.model.InteractionMessage;
import net.socialnetworkstools.bot.model.InteractionResult;
import net.socialnetworkstools.bot.queue.JobService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class GetUserNameCommandHandler extends CommandHandler {

    private static final Logger LOGGER = LogManager.getLogger(GetUserNameCommandHandler.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final JsonPointer POINTER_TO_USERNAME = JsonPointer.compile("/data/user/reel/user/username");

    private final InstaAccountService instaAccountService;
    private final JobService jobService;
    private final I18n i18n;

    public GetUserNameCommandHandler(InstaAccountService instaAccountService, JobService jobService, I18n i18n) {
        this.instaAccountService = instaAccountService;
        this.jobService = jobService;
        this.i18n = i18n;
    }

    @Override
    public Command responsibleFor() {
        return Command.INSTA_GET_USERNAME;
    }

    @Override
    public InteractionResult next(long chatId, String step, String input) {
        GetUserNameStep currentStep = step == null ? GetUserNameStep.START : GetUserNameStep.valueOf(step);
        GetUserNameStep nextStep = (GetUserNameStep) currentStep.next();

        String nextStepName = nextStep.name();
        String currentStepName = currentStep.name();
        if (currentStep.isAsync()) {
            if (jobService.getJobsCount(chatId) > MAX_JOBS_COUNT) {
                String msg = i18n.msg(chatId, "command-handler.too-many-requests");
                return InteractionResult.of(currentStepName, msg);
            }
            jobService.addJob(chatId, responsibleFor(), currentStepName, input);
            String accepted = i18n.msg(chatId, "command-handler.request-accepted");
            return InteractionResult.of(nextStepName, accepted);
        } else {
            return InteractionResult.of(nextStepName, processStep(chatId, currentStepName, input));
        }
    }

    @Override
    public InteractionMessage processStep(long chatId, String step, String input) {
        GetUserNameStep getUserNameStep = GetUserNameStep.valueOf(step);

        Map<GetUserNameStep, Supplier<InteractionMessage>> stepProcessors = Map.of(
            GetUserNameStep.START, () -> InteractionMessage.of(i18n.msg(chatId, "insta.get-username-command-handler.send-me-id")),
            GetUserNameStep.RESOLVE, () -> InteractionMessage.of(resolve(chatId, input)));

        Supplier<InteractionMessage> stepProcessor = stepProcessors.getOrDefault(getUserNameStep,
            () -> {throw new IllegalStateException("Unknown step " + responsibleFor() + ":" + step);}
        );

        return stepProcessor.get();
    }

    private String resolve(long chatId, String userId) {
        String userName = getUserName(userId);
        if (userName == null || userName.isEmpty()) {
            return i18n.msg(chatId, "insta.get-username-command-handler.wrong-id", Text.escape(userId));
        }
        return i18n.msg(chatId, "insta.get-username-command-handler.success", userId, Text.escape(userName));
    }

    private String getUserName(String id) {
        if (isNotId(id)) {
            return null;
        }
        String url = "https://www.instagram.com/graphql/query/"
            + "?query_hash=c9100bf9110dd6361671f113dd02e7d6"
            + "&variables=%7B%22user_id%22:%22" + id + "%22,%22include_chaining%22:false,%22include_reel%22:true,"
            + "%22include_suggested_users%22:false,%22include_logged_out_extras%22:false,%22include_highlight_reels%22:false,"
            + "%22include_related_profiles%22:false"
            + "%7D";

        InstaAccount instaAccount = instaAccountService.get();
        String instaUserName = instaAccount.getUserName();

        Builder requestBuilder = HttpRequest.newBuilder()
            .uri(URI.create(url));

        instaAccount.getHeaders().forEach(header -> requestBuilder.header(header.getName(), header.getValue()));

        HttpRequest request = requestBuilder
            .header("Referer", "https://www.instagram.com/")
            .header("Pragma", "no-cache")
            .header("Cache-Control", "no-cache")
            .timeout(Duration.ofSeconds(10))
            .build();

        JsonNode node;
        try {
            HttpResponse<String> httpResponse = HttpClient.newHttpClient()
                .send(request, BodyHandlers.ofString());

            boolean isNotJsonResponse = Optional.ofNullable(httpResponse.headers())
                .flatMap(headers -> headers.firstValue("Content-Type"))
                .filter(contentType -> contentType.startsWith("application/json"))
                .isEmpty();

            if (isNotJsonResponse) {
                throw new LoggedOutException(instaUserName);
            }

            int statusCode = httpResponse.statusCode();
            if (statusCode == 404) {
                return null;
            }
            if (statusCode != 200) {
                LOGGER.error("Insta respond with {} code. Account: {}", statusCode, instaUserName);
                return null;
            }

            String json = httpResponse.body();

            node = OBJECT_MAPPER.readTree(json);

        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException("Insta account " + instaUserName, e);
        }
        String userName = node.at(POINTER_TO_USERNAME).asText();
        if (userName == null || userName.isEmpty()) {
            LOGGER.trace("User with ID {} doesn't exist or not visible for {}", id, instaUserName);
        } else {
            LOGGER.trace("ID {} resolved to {} on behalf of {}", id, userName, instaUserName);
        }
        return userName;
    }

    private static boolean isNotId(String id) {
        return id == null
            || id.length() < 5
            || id.length() > 12
            || id.chars().anyMatch(ch -> !isDigit(ch));
    }
}
