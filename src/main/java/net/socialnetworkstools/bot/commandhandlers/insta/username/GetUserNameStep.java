package net.socialnetworkstools.bot.commandhandlers.insta.username;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.socialnetworkstools.bot.model.Step;

@Getter
@RequiredArgsConstructor
public enum GetUserNameStep implements Step {
    START(false),
    RESOLVE(true);

    private final boolean async;
}
