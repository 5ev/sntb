package net.socialnetworkstools.bot.commandhandlers.insta.id;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.commandhandlers.insta.username.GetUserNameStep;
import net.socialnetworkstools.bot.exception.LoggedOutException;
import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InstaAccount;
import net.socialnetworkstools.bot.model.InteractionMessage;
import net.socialnetworkstools.bot.model.InteractionResult;
import net.socialnetworkstools.bot.queue.JobService;
import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.service.InstaAccountService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetIdCommandHandler extends CommandHandler {

    private static final Logger LOGGER = LogManager.getLogger(GetIdCommandHandler.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final JsonPointer POINTER_TO_USERNAME = JsonPointer.compile("/data/user/id");

    // https://stackoverflow.com/questions/15470180/character-limit-on-instagram-usernames#17087528
    private static final Pattern USERNAME_PATTERN = Pattern.compile("^(https://(www\\.)?instagram\\.com/)?@?([A-Za-z\\d._]{3,30})/?.*$");

    private final InstaAccountService instaAccountService;
    private final JobService jobService;
    private final I18n i18n;

    public GetIdCommandHandler(InstaAccountService instaAccountService, JobService jobService, I18n i18n) {
        this.instaAccountService = instaAccountService;
        this.jobService = jobService;
        this.i18n = i18n;
    }

    @Override
    public Command responsibleFor() {
        return Command.INSTA_GET_ID;
    }

    @Override
    public InteractionResult next(long chatId, String step, String input) {
        GetIdStep currentStep = step == null ? GetIdStep.START : GetIdStep.valueOf(step);
        GetIdStep nextStep = (GetIdStep) currentStep.next();

        String nextStepName = nextStep.name();
        String currentStepName = currentStep.name();
        if (currentStep.isAsync()) {
            if (jobService.getJobsCount(chatId) > MAX_JOBS_COUNT) {
                String msg = i18n.msg(chatId, "command-handler.too-many-requests");
                return InteractionResult.of(currentStepName, msg);
            }
            jobService.addJob(chatId, responsibleFor(), currentStepName, input);
            String msg = i18n.msg(chatId, "command-handler.request-accepted");
            return InteractionResult.of(nextStepName, msg);
        } else {
            return InteractionResult.of(nextStepName, processStep(chatId, currentStepName, input));
        }
    }

    @Override
    public InteractionMessage processStep(long chatId, String step, String input) {
        GetUserNameStep getUserNameStep = GetUserNameStep.valueOf(step);

        Map<GetUserNameStep, Supplier<InteractionMessage>> stepProcessors = Map.of(
            GetUserNameStep.START, () -> InteractionMessage.of(i18n.msg(chatId, "insta.get-id-command-handler.send-me-username")),
            GetUserNameStep.RESOLVE, () -> InteractionMessage.of(resolve(chatId, input)));

        Supplier<InteractionMessage> stepProcessor = stepProcessors.getOrDefault(getUserNameStep,
            () -> {throw new IllegalStateException("Unknown step " + responsibleFor() + ":" + step);}
        );

        return stepProcessor.get();
    }

    private String resolve(long chatId, String userName) {
        String cleanedUpUserName = cleanUpUserName(userName);
        return Optional.ofNullable(cleanedUpUserName)
            .map(this::getUserId)
            .filter(userId -> !userId.isEmpty())
            .map(userId -> i18n.msg(chatId, "insta.get-id-command-handler.success", cleanedUpUserName, userId))
            .orElseGet(() -> i18n.msg(chatId, "insta.get-id-command-handler.wrong-username", cleanedUpUserName));
    }

    static String cleanUpUserName(String userName) {
        int limit = Math.min(userName.length(), 300);
        String limitedInput = userName.substring(0, limit);
        Matcher matcher = USERNAME_PATTERN.matcher(limitedInput);
        if (matcher.find()) {
            return matcher.group(matcher.groupCount());
        }
        return null;
    }

    private String getUserId(String userName) {

        String url = "https://i.instagram.com/api/v1/users/web_profile_info/?username=" + userName;

        InstaAccount instaAccount = instaAccountService.get();
        String instaUserName = instaAccount.getUserName();

        Builder requestBuilder = HttpRequest.newBuilder()
            .uri(URI.create(url));

        instaAccount.getHeaders().forEach(header -> requestBuilder.header(header.getName(), header.getValue()));

        HttpRequest request = requestBuilder
            .header("Referer", "https://www.instagram.com/")
            .header("Pragma", "no-cache")
            .header("Cache-Control", "no-cache")
            .timeout(Duration.ofSeconds(10))
            .build();

        JsonNode node;
        try {
            HttpResponse<String> httpResponse = HttpClient.newHttpClient()
                .send(request, BodyHandlers.ofString());

            boolean isNotJsonResponse = Optional.ofNullable(httpResponse.headers())
                .flatMap(headers -> headers.firstValue("Content-Type"))
                .filter(contentType -> contentType.startsWith("application/json"))
                .isEmpty();

            if (isNotJsonResponse) {
                throw new LoggedOutException(instaUserName);
            }

            int statusCode = httpResponse.statusCode();
            if (statusCode == 404) {
                return null;
            }
            if (statusCode != 200) {
                LOGGER.error("Insta respond with {} code. Account: {}", statusCode, instaUserName);
                return null;
            }

            String json = httpResponse.body();

            node = OBJECT_MAPPER.readTree(json);

        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException("Insta account " + instaUserName, e);
        }
        String id = node.at(POINTER_TO_USERNAME).asText();
        if (id == null || id.isEmpty()) {
            LOGGER.trace("User {} doesn't exist or not visible for {}", userName, instaUserName);
        } else {
            LOGGER.trace("User {} resolved to {} on behalf of {}", userName, id, instaUserName);
        }
        return id;
    }

}
