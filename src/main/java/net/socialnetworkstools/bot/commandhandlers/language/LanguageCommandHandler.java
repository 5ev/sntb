package net.socialnetworkstools.bot.commandhandlers.language;

import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.service.Text;
import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InteractionMessage;
import net.socialnetworkstools.bot.model.InteractionResult;
import net.socialnetworkstools.bot.model.Language;

public class LanguageCommandHandler extends CommandHandler {

    private final I18n i18n;

    public LanguageCommandHandler(I18n i18n) {
        this.i18n = i18n;
    }

    @Override
    public Command responsibleFor() {
        return Command.LANGUAGE;
    }

    @Override
    public InteractionResult next(long chatId, String step, String input) {
        if (step == null) {
            return InteractionResult.of(LanguageStep.START.name(),
                InteractionMessage.of(i18n.msg(chatId, "language-command-handler.select-lang"), Language.asButtons()));
        }

        String nextStepName = LanguageStep.valueOf(step).next().name();

        String text = Language.find(input)
            .map(language -> {
                i18n.setLang(chatId, language);
                return i18n.msg(chatId, "language-command-handler.hello");
            })
            .orElse(i18n.msg(chatId, "language-command-handler.unsupported", Text.escape(input)));
        return InteractionResult.of(nextStepName, text);
    }
}
