package net.socialnetworkstools.bot.commandhandlers.language;

import net.socialnetworkstools.bot.model.Step;

public enum LanguageStep implements Step {
    START,
    SET_LANG
}
