package net.socialnetworkstools.bot.commandhandlers;

import net.socialnetworkstools.bot.model.Command;
import net.socialnetworkstools.bot.model.InteractionMessage;
import net.socialnetworkstools.bot.model.InteractionResult;

public abstract class CommandHandler {

    protected static final int MAX_JOBS_COUNT = 5;

    public abstract Command responsibleFor();

    public abstract InteractionResult next(long chatId, String step, String input);

    public InteractionMessage processStep(long chatId, String step, String input) {
        throw new IllegalStateException("The method shouldn't be called. Here is no async jobs.");
    }
}
