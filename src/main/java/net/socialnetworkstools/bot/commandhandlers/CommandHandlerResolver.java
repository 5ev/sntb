package net.socialnetworkstools.bot.commandhandlers;

import net.socialnetworkstools.bot.model.Command;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

public class CommandHandlerResolver {

    private final Map<Command, CommandHandler> commandHandlers;

    @Inject
    CommandHandlerResolver(Set<CommandHandler> commandHandlers) {
        this.commandHandlers = commandHandlers.stream()
            .collect(Collectors.toUnmodifiableMap(CommandHandler::responsibleFor, Function.identity()));
    }

    public CommandHandler resolve(Command cmd) {
        return commandHandlers.get(cmd);
    }
}
