package net.socialnetworkstools.bot.commandhandlers;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;
import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.service.InstaAccountService;
import net.socialnetworkstools.bot.commandhandlers.help.HelpCommandHandler;
import net.socialnetworkstools.bot.commandhandlers.insta.id.GetIdCommandHandler;
import net.socialnetworkstools.bot.commandhandlers.insta.username.GetUserNameCommandHandler;
import net.socialnetworkstools.bot.commandhandlers.language.LanguageCommandHandler;
import net.socialnetworkstools.bot.commandhandlers.start.StartCommandHandler;
import net.socialnetworkstools.bot.queue.JobService;

@Module
public class CommandHandlersProvider {

    private CommandHandlersProvider() {
    }

    @Provides
    @IntoSet
    static CommandHandler start(I18n i18n){
        return new StartCommandHandler(i18n);
    }

    @Provides
    @IntoSet
    static CommandHandler instaGetId(InstaAccountService instaAccountService, JobService jobService, I18n i18n){
        return new GetIdCommandHandler(instaAccountService, jobService, i18n);
    }

    @Provides
    @IntoSet
    static CommandHandler instaGetUserName(InstaAccountService instaAccountService, JobService jobService, I18n i18n){
        return new GetUserNameCommandHandler(instaAccountService, jobService, i18n);
    }

    @Provides
    @IntoSet
    static CommandHandler help(I18n i18n){
        return new HelpCommandHandler(i18n);
    }

    @Provides
    @IntoSet
    static CommandHandler language(I18n i18n){
        return new LanguageCommandHandler(i18n);
    }
}
