package net.socialnetworkstools.bot;

import net.socialnetworkstools.bot.model.InteractionMessage;
import net.socialnetworkstools.bot.service.BotService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Bot extends TelegramLongPollingBot {

    private static final Logger LOGGER = LogManager.getLogger(Bot.class);
    private static final int BUTTONS_PER_ROW = 3;

    private final BotService service;
    private final Settings settings;
    private final Object syncObject = new Object();

    @Inject
    Bot(BotService service, Settings settings) {
        this.service = service;
        this.settings = settings;
    }

    @Override
    public String getBotUsername() {
        return "sntoolsbot";
    }

    @Override
    public String getBotToken() {
        return settings.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {

        // Check if the update has a message and the message has text
        final var inputMessage = update.getMessage();
        if (!update.hasMessage() || !inputMessage.hasText() || inputMessage.getChatId() == null) {
            return;
        }

        final var chatId = inputMessage.getChatId();
        final var input = inputMessage.getText();

        final var msg = service.process(chatId, input);

        LOGGER.trace("{}: '{}' -> '{}'", chatId, input, msg.getText());

        shipMessage(chatId, msg);
    }

    public void shipMessage(Long chatId, InteractionMessage interactionMessage) {

        final var outputMessage = SendMessage.builder()
            .chatId(chatId.toString())
            .parseMode(ParseMode.MARKDOWN)
            .disableWebPagePreview(true)
            .text(interactionMessage.getText())
            .replyMarkup(buildButtons(interactionMessage.getButtonTexts()))
            .build();

        try {
            synchronized (syncObject) {
                // Send the message
                execute(outputMessage);
            }
            LOGGER.trace("{}: -> {}", chatId, interactionMessage);
        } catch (TelegramApiException e) {
            LOGGER.error("", e);
        }
    }

    private static ReplyKeyboard buildButtons(String[] texts) {
        if (texts == null || texts.length == 0) {
            // Hide buttons from a previous step if buttons for current step are not needed
            return ReplyKeyboardRemove.builder()
                .removeKeyboard(true)
                .build();
        }

        List<KeyboardRow> rows = new ArrayList<>(1);
        KeyboardRow row = null;
        for(int i = 0; i < texts.length; i++) {
            if (i % BUTTONS_PER_ROW == 0) {
                row = new KeyboardRow();
                rows.add(row);
            }
            row.add(texts[i]);
        }

        // Create a keyboard with buttons
        return ReplyKeyboardMarkup.builder()
            .selective(true)
            .resizeKeyboard(true)
            .selective(true)
            .resizeKeyboard(true)
            .oneTimeKeyboard(true)
            .keyboard(rows)
            .build();
    }
}
