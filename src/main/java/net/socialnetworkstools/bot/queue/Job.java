package net.socialnetworkstools.bot.queue;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.socialnetworkstools.bot.model.Command;

@RequiredArgsConstructor(staticName = "of")
@Getter
@EqualsAndHashCode
public class Job {
    private final long chatId;
    private final Command command;
    private final String step;
    private final String input;

    @Override
    public String toString() {
        return "[" + chatId + ", " + command + ":" + step + ", '" + input + "']";
    }
}
