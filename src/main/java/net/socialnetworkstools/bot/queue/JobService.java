package net.socialnetworkstools.bot.queue;

import net.socialnetworkstools.bot.model.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JobService {

    private static final Logger LOGGER = LogManager.getLogger(JobService.class);
    private final Deque<Job> queue = new ArrayDeque<>();

    @Inject
    JobService() {
    }

    public Optional<Job> getNextJob() {
        Job job;
        synchronized (queue) {
            job = queue.pollFirst();
        }
        if (job != null) {
            LOGGER.trace("Next job: {}", job);
        }
        return Optional.ofNullable(job);
    }

    public void addJob(long chatId, Command command, String step, String input) {
        Job newJob = Job.of(chatId, command, step, input);
        boolean isAppended = false;
        long size = -1;
        synchronized (queue) {
            if (!queue.contains(newJob)) {
                queue.addLast(newJob);
                isAppended = true;
                size = queue.size();
            }
            if (isAppended) {
                LOGGER.trace("New job {} put in queue. Queue size is {}", newJob, size);
            }
        }
    }

    public long getJobsCount(long chatId) {
        long count;
        synchronized (queue) {
            count = queue.stream()
                .filter(jod -> jod.getChatId() == chatId)
                .count();
        }
        LOGGER.trace("{}: queue has {} jobs", chatId, count);
        return count;
    }
}
