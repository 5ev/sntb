package net.socialnetworkstools.bot.queue;

import net.socialnetworkstools.bot.Bot;
import net.socialnetworkstools.bot.service.I18n;
import net.socialnetworkstools.bot.service.InstaAccountService;
import net.socialnetworkstools.bot.exception.LoggedOutException;
import net.socialnetworkstools.bot.exception.OutOfServiceException;
import net.socialnetworkstools.bot.commandhandlers.CommandHandler;
import net.socialnetworkstools.bot.commandhandlers.CommandHandlerResolver;
import net.socialnetworkstools.bot.model.InteractionMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;

public class Worker {

    private static final Logger LOGGER = LogManager.getLogger(Worker.class);

    private final JobService jobService;
    private final Bot bot;
    private final CommandHandlerResolver commandHandlerResolver;
    private final InstaAccountService instaAccountService;
    private final I18n i18n;

    @Inject
    public Worker(JobService jobService, Bot bot, CommandHandlerResolver commandHandlerResolver, InstaAccountService instaAccountService,
        I18n i18n) {

        this.jobService = jobService;
        this.bot = bot;
        this.commandHandlerResolver = commandHandlerResolver;
        this.instaAccountService = instaAccountService;
        this.i18n = i18n;
    }

    public void processJob() {
        jobService.getNextJob().ifPresent(job -> {
            CommandHandler commandHandler = commandHandlerResolver.resolve(job.getCommand());
            InteractionMessage interactionMessage;
            while (true) {
                try {
                    interactionMessage = commandHandler.processStep(job.getChatId(), job.getStep(), job.getInput());
                    break;
                } catch (LoggedOutException e) {
                    instaAccountService.deactivate(e.getUserName());
                    // try again with another user
                } catch (OutOfServiceException e) {
                    interactionMessage = InteractionMessage.of(i18n.msg(job.getChatId(), "out-of-service"));
                    break;
                }
            }
            bot.shipMessage(job.getChatId(), interactionMessage);
            LOGGER.trace("Job {} processed", job);
        });
    }
}
