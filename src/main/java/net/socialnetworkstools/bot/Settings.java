package net.socialnetworkstools.bot;

import javax.inject.Inject;

/** Provides the application settings */
public class Settings {

    @Inject
    Settings() {
    }

    /**
     * Returns the bot's token read from system property.
     *
     * @throws IllegalStateException if the token is not found
     */
    public String getToken() {
        final var token = System.getProperty("token");
        if (token == null) {
            throw new IllegalStateException("System property 'token' is not found");
        }
        return token;
    }

    private static String getSystemProperty(String name) {
        final var token = System.getProperty(name);
        if (token == null) {
            throw new IllegalStateException("System property '" + name + "' is not found");
        }
        return token;
    }

    public String getDbUrl() {
        return getSystemProperty("dbUrl");
    }

    public String getDbUser() {
        return getSystemProperty("dbUsr");
    }

    public String getDbPassword() {
        return getSystemProperty("dbPsw");
    }
}
