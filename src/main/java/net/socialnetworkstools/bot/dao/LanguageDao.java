package net.socialnetworkstools.bot.dao;

import lombok.NonNull;
import net.socialnetworkstools.bot.model.Language;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LanguageDao {

    private static final String DEFAULT_LANG = Language.RU.name().toLowerCase(Locale.ROOT);

    private final Map<Long, String> db = new ConcurrentHashMap<>();

    @Inject
    LanguageDao() {
    }

    @NonNull
    public String getLang(long chatId) {
        return db.getOrDefault(chatId, DEFAULT_LANG);
    }

    public void setLang(long chatId, @NonNull String language) {
        db.put(chatId, language);
    }
}
