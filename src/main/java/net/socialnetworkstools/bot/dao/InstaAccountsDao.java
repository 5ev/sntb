package net.socialnetworkstools.bot.dao;

import net.socialnetworkstools.bot.Settings;
import net.socialnetworkstools.bot.model.InstaAccount;
import net.socialnetworkstools.bot.model.InstaAccount.Header;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class InstaAccountsDao {

    private final List<InstaAccount> db;

    @Inject
    public InstaAccountsDao(Settings settings) {

        String dbUrl = settings.getDbUrl();
        String dbUsr = settings.getDbUser();
        String dbPsw = settings.getDbPassword();

        List<InstaAccount> accounts = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUsr, dbPsw);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("select * from insta_accounts")) {

            while (resultSet.next()) {
                String userName = resultSet.getString("user_name");
                List<Header> headers = loadHeaders(connection, userName);
                accounts.add(InstaAccount.of(userName, headers));
            }

        } catch (SQLException e) {
            throw new IllegalStateException("Database issue", e);
        }
        db = accounts;
    }

    private static List<InstaAccount.Header> loadHeaders(Connection connection, String userName) throws SQLException {
        String sql = "select * from headers where insta_account_name = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userName);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<InstaAccount.Header> headers = new ArrayList<>();

                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    String value = resultSet.getString("value");
                    headers.add(InstaAccount.Header.of(name, value));
                }
                return headers;
            }
        }
    }

    public List<InstaAccount> getInstaAccounts() {
        return db;
    }
}
