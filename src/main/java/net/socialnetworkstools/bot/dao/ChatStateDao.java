package net.socialnetworkstools.bot.dao;

import net.socialnetworkstools.bot.model.ChatState;
import net.socialnetworkstools.bot.model.Command;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ChatStateDao {

    private final Map<Long, ChatState> db = new ConcurrentHashMap<>();

    @Inject
    ChatStateDao() {
    }

    public Optional<ChatState> getChatState(long chatId) {
        return Optional.ofNullable(db.get(chatId));
    }

    public void setChatState(long chatId, Command command, String state) {
        db.put(chatId, ChatState.of(command, state));
    }
}
