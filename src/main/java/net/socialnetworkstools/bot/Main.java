package net.socialnetworkstools.bot;

import net.socialnetworkstools.bot.queue.Worker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    /** Main method */
    public static void main(String[] args) {
        try {
            BotFactory botFactory = DaggerBotFactory.create();

            Worker worker = botFactory.worker();
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            executor.scheduleAtFixedRate(worker::processJob, 5, 30, TimeUnit.SECONDS);

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                executor.shutdown();
                LOGGER.info("Bot stopped");
            }));

            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(botFactory.bot());

            LOGGER.info("Bot started");
        } catch (Exception e) {
            LOGGER.error("Unexpected error", e);
        }
    }
}
