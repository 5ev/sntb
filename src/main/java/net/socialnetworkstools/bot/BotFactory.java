package net.socialnetworkstools.bot;

import dagger.Component;
import net.socialnetworkstools.bot.commandhandlers.CommandHandlersProvider;
import net.socialnetworkstools.bot.queue.Worker;

import javax.inject.Singleton;

@Singleton
@Component(modules = {CommandHandlersProvider.class})
public interface BotFactory {

    Bot bot();

    Worker worker();
}
