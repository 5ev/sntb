package net.socialnetworkstools.bot;

import static org.assertj.core.api.Assertions.assertThat;

import net.socialnetworkstools.bot.model.Step;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class StepTest {

    private enum TestEnum implements Step {
        ITEM1,
        ITEM2,
        ITEM3,
        ITEM4;

        public boolean isAsync() {
            return false;
        }
    }

    @ParameterizedTest
    @CsvSource({
        "ITEM1, ITEM2",
        "ITEM2, ITEM3",
        "ITEM3, ITEM4",
        "ITEM4, ITEM2"
    })
    void next(TestEnum current, TestEnum expectedNext) {
        assertThat(current.next()).isSameAs(expectedNext);
    }
}