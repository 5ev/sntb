package net.socialnetworkstools.bot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import net.socialnetworkstools.bot.dao.LanguageDao;
import net.socialnetworkstools.bot.service.I18n;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.MissingResourceException;

@ExtendWith(MockitoExtension.class)
class I18nTest {

    @InjectMocks
    private I18n i18n;

    @Mock
    private LanguageDao languageDao;

    @ParameterizedTest
    @CsvSource({
        "ru, 'Добро пожаловать'",
        "en, Welcome"
    })
    void test(String lang, String expected) {

        when(languageDao.getLang(11)).thenReturn(lang);
        String message = i18n.msg(11, "help.welcome.label");

        assertThat(message).isEqualTo(expected);
    }

    @Test
    void argsEn() {
        when(languageDao.getLang(11)).thenReturn("en");
        String message = i18n.msg(11, "test.greeting", "Dear Martha", 100);

        assertThat(message).isEqualTo("Hello Dear Martha, take $100!");
    }

    @Test
    void argsRuSingular() {
        when(languageDao.getLang(11)).thenReturn("ru");
        String message = i18n.msg(11, "test.greeting", "Сергей", 1);

        assertThat(message).isEqualTo("Привет, Сергей, возьми 1 рубль!");
    }

    @Test
    void argsRuPlural() {
        when(languageDao.getLang(11)).thenReturn("ru");
        String message = i18n.msg(11, "test.greeting", "Сергей", 100);

        assertThat(message).isEqualTo("Привет, Сергей, возьми 100 рублей!");
    }

    @Test
    void invalidKey() {
        when(languageDao.getLang(11)).thenReturn("en");
        assertThatThrownBy(() -> i18n.msg(11, "invalid.key"))
            .isInstanceOf(MissingResourceException.class)
            .hasMessage("Can't find resource for bundle java.util.PropertyResourceBundle, key invalid.key");
    }
}