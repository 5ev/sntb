package net.socialnetworkstools.bot.commandhandlers.insta.id;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;

class GetIdCommandHandlerTest {

    @ParameterizedTest
    @CsvSource({
        "https://instagram.com/_teSt.17_, _teSt.17_",
        "https://instagram.com/_teSt.17_/, _teSt.17_",
        "https://instagram.com/_teSt.17_/?jhgjh=yyy&iii=17, _teSt.17_",
        "https://www.instagram.com/_teSt.17_/?jhgjh=yyy&iii=17, _teSt.17_",
        "@_teSt.17_, _teSt.17_",
        "_teSt.17_, _teSt.17_",
        "***,",
        "'',",
    })
    void cleanUpUserName(String input, String expected) {
        String actual = GetIdCommandHandler.cleanUpUserName(input);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void cleanUpSuperLongUserName() {
        char[] chars = new char[350];
        Arrays.fill(chars, 'x');
        String input = "https://www.instagram.com/" + new String(chars);
        String actual = GetIdCommandHandler.cleanUpUserName(input);
        assertThat(actual).isEqualTo("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"); //30 chars max
    }
}