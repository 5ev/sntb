package net.socialnetworkstools.bot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import net.socialnetworkstools.bot.dao.InstaAccountsDao;
import net.socialnetworkstools.bot.model.InstaAccount;
import net.socialnetworkstools.bot.service.InstaAccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class InstaAccountServiceTest {

    @Mock
    private InstaAccountsDao dao;

    @Test
    void get() {
        List<InstaAccount> accounts = List.of(
            InstaAccount.of("user1",
                List.of(InstaAccount.Header.of("X-IG-App-ID" ,"111111111111111"))),
            InstaAccount.of("user2",
                List.of(InstaAccount.Header.of("Cookie", "csrftoken=JFLKJDUhfkjKJlkjHhyy")))
        );
        when(dao.getInstaAccounts()).thenReturn(accounts);
        InstaAccountService service = new InstaAccountService(dao);

        assertThat(service.get())
            .usingRecursiveComparison()
            .isEqualTo(accounts.get(1));

        assertThat(service.get())
            .usingRecursiveComparison()
            .isEqualTo(accounts.get(0));

        assertThat(service.get())
            .usingRecursiveComparison()
            .isEqualTo(accounts.get(1));
    }
}