package net.socialnetworkstools.bot.queue;

import static org.assertj.core.api.Assertions.assertThat;

import net.socialnetworkstools.bot.model.Command;
import org.junit.jupiter.api.Test;

class JobServiceTest {

    @Test
    void testCount() {
        JobService jobService = new JobService();

        jobService.addJob(1, Command.START, "START", "start");
        jobService.addJob(1, Command.HELP, "START", "help");

        jobService.addJob(2, Command.START, "START", "start");
        jobService.addJob(2, Command.INSTA_GET_ID, "START", "getid");
        jobService.addJob(2, Command.INSTA_GET_USERNAME, "START", "getusername");

        assertThat(jobService.getJobsCount(1)).isEqualTo(2);
        assertThat(jobService.getJobsCount(2)).isEqualTo(3);
    }

    @Test
    void getJob() {
        JobService jobService = new JobService();

        jobService.addJob(2, Command.HELP, "START", "help");
        jobService.addJob(1, Command.START, "START", "start");
        jobService.addJob(2, Command.INSTA_GET_ID, "START", "getid");

        assertThat(jobService.getNextJob())
            .isNotEmpty()
            .get()
            .usingRecursiveComparison()
            .isEqualTo(Job.of(2, Command.HELP, "START", "help"));

        assertThat(jobService.getJobsCount(1)).isEqualTo(1);
        assertThat(jobService.getJobsCount(2)).isEqualTo(1);
    }
}