#!/bin/sh

echo "$INSTA_ACCOUNTS" > "$PWD/insta-accounts.json"
java $JAVA_OPTS \
 "-DmyInstaAccounts=$PWD/insta-accounts.json" \
 "-DdbUrl=$DB_URL" \
 "-DdbUsr=$DB_USR" \
 "-DdbPsw=$DB_PSW" \
 "-Dtoken=$TOKEN" \
 -jar \
 target/bot-1.0-jar-with-dependencies.jar